# LinkqPriCocoaPod

[![CI Status](https://img.shields.io/travis/rooster2578@gmail.com/LinkqPriCocoaPod.svg?style=flat)](https://travis-ci.org/rooster2578@gmail.com/LinkqPriCocoaPod)
[![Version](https://img.shields.io/cocoapods/v/LinkqPriCocoaPod.svg?style=flat)](https://cocoapods.org/pods/LinkqPriCocoaPod)
[![License](https://img.shields.io/cocoapods/l/LinkqPriCocoaPod.svg?style=flat)](https://cocoapods.org/pods/LinkqPriCocoaPod)
[![Platform](https://img.shields.io/cocoapods/p/LinkqPriCocoaPod.svg?style=flat)](https://cocoapods.org/pods/LinkqPriCocoaPod)

It's a demo of CageLin Learning to build a library based on CocoaPod.

This pod includes five methods, they can be used to log string to console, add a UIView, add a xib file, add a image to
screen.

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Usage

```
#import <LinkqPriCocoaPod/MDNetwork.h>

MDNetwrok *_work;

//NSLog test
[_work logOneWord];
    
//Network Test
[_work getURLWith:@"https://beta.apple.com/sp/zh/betaprogram/welcome?locale=zh"];
    
//UI Test
UIView *view = [_work returnOneBlueView];
view.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height *2/3);
[self.view addSubview:view];
    
//Button Test
UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 50, 300, 100)];
imageView.image = [_work addOneImage];
[self.view addSubview:imageView];
UIButton *btnView = [_work addOneButton];
btnView.frame = CGRectMake(50, self.view.frame.size.height/2, 150, 50);
[self.view addSubview:btnView];
```

## Installation

LinkqPriCocoaPod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LinkqPriCocoaPod'
```

## Author

rooster2578@gmail.com

## License

LinkqPriCocoaPod is available under the MIT license. See the LICENSE file for more info.


